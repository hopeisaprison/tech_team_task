package com.hopeisaprison.ru.esctechtest

import android.app.Application
import com.hopeisaprison.ru.esctechtest.di.components.ActivityComponent
import com.hopeisaprison.ru.esctechtest.di.components.AppComponent
import com.hopeisaprison.ru.esctechtest.di.components.DaggerActivityComponent
import com.hopeisaprison.ru.esctechtest.di.components.DaggerAppComponent
import com.hopeisaprison.ru.esctechtest.di.modules.NavigationModule
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class App : Application() {

    companion object {
        lateinit var INSTANCE: App
    }

//    val activityComponent: ActivityComponent by lazy {
//        .builder().build()
//    }

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().navigationModule(NavigationModule(Cicerone.create())).build()
    }


    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

    }







}