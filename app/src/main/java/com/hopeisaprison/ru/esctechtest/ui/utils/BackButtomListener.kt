package com.hopeisaprison.ru.esctechtest.ui.utils

interface BackButtomListener {

    fun onBackClick()
}