package com.hopeisaprison.ru.esctechtest.di.components

import com.hopeisaprison.ru.esctechtest.di.modules.NavigationModule
import com.hopeisaprison.ru.esctechtest.ui.main.MainActivity
import com.hopeisaprison.ru.moxysample.ui.base.BaseActivity
import dagger.Component
import dagger.Provides
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Singleton
@Component(modules = [NavigationModule::class])
interface AppComponent {

    fun router(): Router

    fun navigatorHolder(): NavigatorHolder

}