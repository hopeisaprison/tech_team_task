package com.hopeisaprison.ru.esctechtest.di.components

import com.hopeisaprison.ru.esctechtest.di.PerActivity
import com.hopeisaprison.ru.esctechtest.di.modules.ActivityModule
import com.hopeisaprison.ru.esctechtest.di.modules.NavigationModule
import com.hopeisaprison.ru.esctechtest.ui.main.MainActivity
import dagger.Component
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@PerActivity
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {


    fun inject(activity: MainActivity)
}