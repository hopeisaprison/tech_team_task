package com.hopeisaprison.ru.esctechtest

class Screens {

    companion object {
        val PROVIDERS_SCREEN = "providers screen"

        val GIFT_DETAIL_SCREEN = "gift detail screen"
    }
}