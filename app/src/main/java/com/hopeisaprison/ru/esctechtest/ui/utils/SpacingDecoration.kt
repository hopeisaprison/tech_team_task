package com.hopeisaprison.ru.esctechtest.ui.utils

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View

class SpacingDecoration(val offsetInPx: Int) : RecyclerView.ItemDecoration() {



    override fun getItemOffsets(outRect: Rect?, view: View, parent: RecyclerView?, state: RecyclerView.State?) {
        val adapterPosition = parent?.getChildAdapterPosition(view)
        var top = 0
        var bottom = 0
        var start = 0
        var end = 0


        if (parent?.layoutManager is LinearLayoutManager) {
            if ((parent.layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.HORIZONTAL) {
                top = offsetInPx
                bottom = offsetInPx
                if (adapterPosition == 0)
                    start = offsetInPx
                end = offsetInPx
            }
            else if ((parent.layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.VERTICAL){
                if (adapterPosition == 0)
                    top = offsetInPx

                bottom = offsetInPx
                start = offsetInPx
                end = offsetInPx
            }
        }

        start -= view.getPaddingLeft()
        top -= view.getPaddingTop()
        end -= view.getPaddingRight()
        bottom -= view.getPaddingBottom()

        outRect?.set(start, top, end, bottom)

    }
}