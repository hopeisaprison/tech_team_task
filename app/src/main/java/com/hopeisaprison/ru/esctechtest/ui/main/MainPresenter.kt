package com.hopeisaprison.ru.esctechtest.ui.main

import com.arellomobile.mvp.InjectViewState
import com.hopeisaprison.ru.esctechtest.App
import com.hopeisaprison.ru.esctechtest.Screens
import com.hopeisaprison.ru.moxysample.ui.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(val router: Router, compositeDisposable: CompositeDisposable) : BasePresenter<IMainView>(compositeDisposable) {

    override fun init() {
        router.navigateTo(Screens.PROVIDERS_SCREEN)
    }


}