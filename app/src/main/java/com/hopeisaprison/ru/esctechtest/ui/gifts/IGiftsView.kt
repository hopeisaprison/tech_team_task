package com.hopeisaprison.ru.esctechtest.ui.gifts

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftProvider
import com.hopeisaprison.ru.moxysample.ui.base.IBaseView

interface IGiftsView: MvpView {


    @StateStrategyType(AddToEndStrategy::class)
    fun showProgress()

    @StateStrategyType(AddToEndStrategy::class)
    fun hideProgress()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showGiftProviders(providers: List<GiftProvider>)
}