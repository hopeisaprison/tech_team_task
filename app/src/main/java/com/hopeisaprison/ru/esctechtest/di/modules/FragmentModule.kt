package com.hopeisaprison.ru.esctechtest.di.modules

import com.hopeisaprison.ru.esctechtest.data.network.ApiEndPoint
import com.hopeisaprison.ru.esctechtest.data.network.GiftsApi
import com.hopeisaprison.ru.esctechtest.di.PerActivity
import com.hopeisaprison.ru.esctechtest.di.PerFragment
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class FragmentModule {

    @Provides
    fun converterFactory() = GsonConverterFactory.create()

    @Provides
    fun rxJavaConverterFactory() = RxJava2CallAdapterFactory.create()

    @Provides
    @PerFragment
    fun retrofit(gsonConverterFactory: GsonConverterFactory,
                 rxJava2CallAdapterFactory: RxJava2CallAdapterFactory) =
            Retrofit.Builder()
                    .baseUrl(ApiEndPoint.API_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(rxJava2CallAdapterFactory)
                    .build()


    @Provides
    @PerFragment
    fun compositeDisposable() = CompositeDisposable()

    @Provides
    @PerFragment
    fun giftsApi(retrofit: Retrofit) = retrofit.create(GiftsApi::class.java)

}