package com.hopeisaprison.ru.esctechtest.ui.main

import android.os.Bundle
import android.support.transition.Fade
import android.support.transition.TransitionInflater
import android.support.transition.TransitionSet
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hopeisaprison.ru.esctechtest.App
import com.hopeisaprison.ru.esctechtest.R
import com.hopeisaprison.ru.esctechtest.Screens
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftCard
import com.hopeisaprison.ru.esctechtest.di.components.DaggerActivityComponent
import com.hopeisaprison.ru.esctechtest.ui.description.DescriptionFragment
import com.hopeisaprison.ru.esctechtest.ui.gifts.GiftsFragment
import com.hopeisaprison.ru.esctechtest.ui.utils.BackButtomListener
import com.hopeisaprison.ru.moxysample.ui.base.BaseActivity
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import ru.terrakok.cicerone.commands.Command
import java.security.AccessController.getContext
import javax.inject.Inject

class MainActivity : BaseActivity(), IMainView {



    @Inject
    lateinit var daggerPresenter: MainPresenter
        @ProvidePresenter get

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter


    private val navigator = object: SupportFragmentNavigator(supportFragmentManager, R.id.main_container) {

        override fun exit() {
           finish()
        }



        override fun createFragment(screenKey: String?, data: Any?): Fragment? {
            return when (screenKey) {
                Screens.PROVIDERS_SCREEN -> GiftsFragment()
                Screens.GIFT_DETAIL_SCREEN -> DescriptionFragment.newInstance(data as GiftCard)
                else -> {
                    null
                }
            }
        }

        override fun showSystemMessage(message: String?) {
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        }

        override fun setupFragmentTransactionAnimation(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction) {
            if (currentFragment is GiftsFragment && nextFragment is DescriptionFragment) {
                setupTransitions(fragmentTransaction, currentFragment, nextFragment)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerActivityComponent.builder()
                .appComponent(App.INSTANCE.appComponent)
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter.init()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.main_container)
        if (supportFragmentManager.backStackEntryCount >1 && fragment != null && fragment is BackButtomListener) {
            (fragment as BackButtomListener).onBackClick()
        }
        else
            finish()

    }

    override fun getNavigator() = navigator

    val FADE_DEFAULT_TIME = 300.toLong()
    val MOVE_DEFAULT_TIME = 1000.toLong()

    fun setupTransitions(fragmentTransaction: FragmentTransaction, previousFragment: GiftsFragment,
                         nextFragment: DescriptionFragment) {

        val transition  =
                TransitionInflater.from(this).inflateTransition(R.transition.transition_shared_element)

        previousFragment.sharedElementReturnTransition = transition
        nextFragment.sharedElementEnterTransition = transition
//        previousFragment.exitTransition =  Fade().apply {
//            duration = FADE_DEFAULT_TIME
//        }
//
//
//        nextFragment.sharedElementEnterTransition = TransitionSet().apply {
//            addTransition(TransitionInflater.from(this@MainActivity).inflateTransition(android.R.transition.move))
//            duration = MOVE_DEFAULT_TIME
//            startDelay = FADE_DEFAULT_TIME
//        }
//
//        nextFragment.enterTransition = Fade().apply {
//            startDelay = MOVE_DEFAULT_TIME + FADE_DEFAULT_TIME
//            duration = FADE_DEFAULT_TIME
//        }
//
//        nextFragment.sharedElementReturnTransition = TransitionSet().apply {
//
//        }




        val transitionView = previousFragment.giftCardViewForAnimation

        nextFragment.transitionName = transitionView!!.transitionName



        fragmentTransaction.addSharedElement(transitionView, transitionView.transitionName)
    }
}
