package com.hopeisaprison.ru.moxysample.ui.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by hopeisaprison on 04.03.18.
 */
abstract class BasePresenter<T: MvpView> constructor(val compositeDisposable: CompositeDisposable) : MvpPresenter<T>() {


    abstract fun init()

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }
}