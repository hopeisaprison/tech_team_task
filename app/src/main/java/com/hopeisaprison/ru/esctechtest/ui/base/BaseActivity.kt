package com.hopeisaprison.ru.moxysample.ui.base


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.hopeisaprison.ru.esctechtest.App
import com.hopeisaprison.ru.esctechtest.R
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import javax.inject.Inject

/**
 * Created by hopeisaprison on 24.02.18.
 */
abstract class BaseActivity: MvpAppCompatActivity(), IBaseView {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder


    override fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showError(msg: String) {
        val snackbar = Snackbar.make(findViewById<View>(android.R.id.content),
                msg, Snackbar.LENGTH_SHORT)

        snackbar.show()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    abstract fun getNavigator(): Navigator






}