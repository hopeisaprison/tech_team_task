package com.hopeisaprison.ru.esctechtest.ui.gifts

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hopeisaprison.ru.esctechtest.App
import com.hopeisaprison.ru.esctechtest.R
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftCard
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftProvider
import com.hopeisaprison.ru.esctechtest.di.components.DaggerFragmentComponent
import com.hopeisaprison.ru.esctechtest.ui.base.BaseFragment
import com.hopeisaprison.ru.esctechtest.ui.utils.BackButtomListener
import com.hopeisaprison.ru.esctechtest.ui.utils.SpacingDecoration
import com.hopeisaprison.ru.esctechtest.ui.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_gifts.*
import kotlinx.android.synthetic.main.fragment_gifts.view.*
import kotlinx.android.synthetic.main.item_gift.view.*
import javax.inject.Inject

class GiftsFragment : BaseFragment(), IGiftsView, BackButtomListener {


    @Inject
    lateinit var daggerPresenter: GiftsPresenter
        @ProvidePresenter get

    @InjectPresenter
    lateinit var mainPresenter: GiftsPresenter

    var giftCardViewForAnimation: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerFragmentComponent.builder()
                .appComponent(App.INSTANCE.appComponent)
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)

        mainPresenter.init()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_gifts, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initViews()
    }
    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onBackClick() {
        mainPresenter.onBackClick()
    }


    override fun hideProgress() {
        progress_bar.visibility = View.INVISIBLE
    }

    fun initViews() {
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.addItemDecoration(SpacingDecoration(UIUtils.dpToPx(context, 8)))
        recycler_view.adapter = ProvidersAdapter().apply {
            onCardClickListener = View.OnClickListener {
                val transitionName = context?.getString(R.string.transitionShop)

                ViewCompat.setTransitionName(it.image_view_avatar, transitionName)

                giftCardViewForAnimation = it.image_view_avatar
                mainPresenter.onCardDetailsClick(it.tag as GiftCard)
            }
        }
    }

    override fun showGiftProviders(providers: List<GiftProvider>) {
        (recycler_view.adapter as ProvidersAdapter).providers = providers
    }


}