package com.hopeisaprison.ru.esctechtest.ui.description

import android.content.Context
import android.os.Bundle
import android.support.transition.Transition
import android.support.transition.TransitionInflater
import android.support.transition.TransitionSet
import android.support.v4.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.hopeisaprison.ru.esctechtest.App
import com.hopeisaprison.ru.esctechtest.R
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftCard
import com.hopeisaprison.ru.esctechtest.di.components.DaggerFragmentComponent
import com.hopeisaprison.ru.esctechtest.ui.base.BaseFragment
import com.hopeisaprison.ru.esctechtest.ui.utils.BackButtomListener
import kotlinx.android.synthetic.main.fragment_card_description.view.*
import javax.inject.Inject

class DescriptionFragment: BaseFragment(), IDescriptionView, BackButtomListener {




    companion object {
        private val TAG_GIFT_CARD = "gift card"

        fun newInstance(card: GiftCard): DescriptionFragment {
            val bundle = Bundle()

            bundle.putSerializable(TAG_GIFT_CARD, card)

            val fragment = DescriptionFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    var transitionName= ""

    val transitionListener =  object: Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition) {
            initViews()
        }

        override fun onTransitionResume(transition: Transition) {

        }

        override fun onTransitionPause(transition: Transition) {

        }

        override fun onTransitionCancel(transition: Transition) {

        }

        override fun onTransitionStart(transition: Transition) {
            val giftCard = arguments?.get(TAG_GIFT_CARD) as GiftCard

            Glide.with(context!!)
                    .load(giftCard.imageUrl)
                    .into(view!!.image_view_avatar_big)
        }

    }

    @Inject
    lateinit var daggerPresenter: DescriptionPresenter
        @ProvidePresenter get

    @InjectPresenter
    lateinit var descriptionPresenter: DescriptionPresenter


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (sharedElementEnterTransition is TransitionSet) {
            (sharedElementEnterTransition as TransitionSet).addListener(transitionListener)
        }
    }

    override fun onDetach() {
        super.onDetach()

        if (sharedElementEnterTransition is TransitionSet) {
            (sharedElementEnterTransition as TransitionSet).removeListener(transitionListener)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerFragmentComponent.builder()
                .appComponent(App.INSTANCE.appComponent)
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_card_description, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setTransitionName(view.image_view_avatar_big, transitionName)

    }

    override fun onBackClick() {
        descriptionPresenter.onBackClick()
    }






    private fun initViews() {
        val giftCard = arguments?.get(TAG_GIFT_CARD) as GiftCard

        Glide.with(context!!)
                .load(giftCard.imageUrl)
                .into(view!!.image_view_avatar_big)

        view?.tv_title?.text = giftCard.title



    }
}