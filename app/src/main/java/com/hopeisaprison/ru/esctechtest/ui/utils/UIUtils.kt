package com.hopeisaprison.ru.esctechtest.ui.utils

import android.content.Context
import android.util.TypedValue

class UIUtils {

    companion object {
        fun dpToPx(context: Context?, dp: Int) =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat() ,context?.resources?.displayMetrics).toInt()

    }
}