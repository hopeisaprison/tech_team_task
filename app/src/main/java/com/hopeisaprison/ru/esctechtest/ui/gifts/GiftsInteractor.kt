package com.hopeisaprison.ru.esctechtest.ui.gifts

import com.hopeisaprison.ru.esctechtest.data.network.GiftsApi
import com.hopeisaprison.ru.moxysample.ui.base.BaseInteractor
import javax.inject.Inject

class GiftsInteractor @Inject constructor(api: GiftsApi): BaseInteractor(api) {

    fun getGiftProviders() = api.getGiftProviders()
}