package com.hopeisaprison.ru.esctechtest.ui.gifts

import android.util.Log
import android.view.View
import com.arellomobile.mvp.InjectViewState
import com.hopeisaprison.ru.esctechtest.Screens
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftCard
import com.hopeisaprison.ru.moxysample.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class GiftsPresenter @Inject constructor(val router: Router,
                                         val giftsInteractor: GiftsInteractor,
                                         compositeDisposable: CompositeDisposable) :
        BasePresenter<IGiftsView>(compositeDisposable) {

    override fun init() {
        viewState.showProgress()
        compositeDisposable.add(giftsInteractor.getGiftProviders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.hideProgress()
                    viewState.showGiftProviders(it.providers)
                }, {
                    it.printStackTrace()
                    viewState.hideProgress()
                }))
    }

    fun onBackClick() {
        router.exit()
    }

    fun onCardDetailsClick(giftCard: GiftCard) {
        router.navigateTo(Screens.GIFT_DETAIL_SCREEN, giftCard)
    }
}