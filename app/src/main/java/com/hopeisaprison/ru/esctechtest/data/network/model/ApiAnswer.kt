package com.hopeisaprison.ru.esctechtest.data.network.model

import com.google.gson.annotations.SerializedName

data class ApiAnswer (
        @SerializedName("providers") val providers: List<GiftProvider>
)