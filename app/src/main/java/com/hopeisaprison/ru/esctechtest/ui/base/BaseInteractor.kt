package com.hopeisaprison.ru.moxysample.ui.base


import com.hopeisaprison.ru.esctechtest.data.network.GiftsApi
import javax.inject.Inject

/**
 * Created by hopeisaprison on 25.03.18.
 */
open class BaseInteractor @Inject constructor(val api: GiftsApi) {

}