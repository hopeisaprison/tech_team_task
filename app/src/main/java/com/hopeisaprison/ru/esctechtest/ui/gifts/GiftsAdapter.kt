package com.hopeisaprison.ru.esctechtest.ui.gifts

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hopeisaprison.ru.esctechtest.R
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftCard
import kotlinx.android.synthetic.main.item_gift.view.*

class GiftsAdapter(val gifts: List<GiftCard>) : RecyclerView.Adapter<GiftsAdapter.GiftHolder>() {


    var onClickListener: View.OnClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiftHolder {
        return GiftHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_gift, parent, false))
    }

    override fun getItemCount() =  gifts.size


    override fun onBindViewHolder(holder: GiftHolder, position: Int) {
       holder.bind(gifts[position])
    }




    inner class GiftHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(card: GiftCard) {
            itemView.text_view_credits.text = "${card.credits} ${card.currency}"

            itemView.text_view_coins.text = card.title

            Glide.with(itemView.context)
                    .load(card.imageUrl)
                    .into(itemView.image_view_avatar)



            itemView.setOnClickListener(onClickListener)

            itemView.tag = card
        }
    }
}