package com.hopeisaprison.ru.esctechtest.ui.gifts

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hopeisaprison.ru.esctechtest.R
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftProvider
import com.hopeisaprison.ru.esctechtest.ui.utils.SpacingDecoration
import com.hopeisaprison.ru.esctechtest.ui.utils.UIUtils
import kotlinx.android.synthetic.main.cardview_provider.view.*
import kotlinx.android.synthetic.main.fragment_gifts.view.*

class ProvidersAdapter() : RecyclerView.Adapter<ProvidersAdapter.ProviderHolder>() {


    var providers: List<GiftProvider> = ArrayList()
     set(value) {
         field = value
         notifyDataSetChanged()
     }

    var onCardClickListener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderHolder {
        return ProviderHolder(LayoutInflater.from(parent.context).inflate(R.layout.cardview_provider, parent, false))
    }

    override fun getItemCount() = providers.size


    override fun onBindViewHolder(holder: ProviderHolder, position: Int) {
        holder.bind(providers[position])
    }



    inner class ProviderHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        init {
            itemView.recycler_view_gifts.layoutManager =
                    LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
            itemView.recycler_view_gifts.addItemDecoration(SpacingDecoration(UIUtils.dpToPx(itemView.context, 8)))
        }

        fun bind(giftProvider: GiftProvider) {
            itemView.tv_title.text = giftProvider.title


            itemView.recycler_view_gifts.adapter = GiftsAdapter(giftProvider.giftCards).apply {
                onClickListener = onCardClickListener
            }



        }
    }
}