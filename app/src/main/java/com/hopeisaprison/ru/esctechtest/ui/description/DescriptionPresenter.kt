package com.hopeisaprison.ru.esctechtest.ui.description

import com.hopeisaprison.ru.moxysample.ui.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class DescriptionPresenter @Inject constructor(val router: Router, compositeDisposable: CompositeDisposable) : BasePresenter<IDescriptionView>(compositeDisposable) {



    override fun init() {

    }

    fun onBackClick() {
        router.exit()
    }
}