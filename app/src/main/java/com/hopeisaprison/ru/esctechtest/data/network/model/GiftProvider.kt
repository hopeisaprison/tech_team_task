package com.hopeisaprison.ru.esctechtest.data.network.model

import com.google.gson.annotations.SerializedName

data class GiftProvider (
        @SerializedName("id") val id: Int,
        @SerializedName("title") val title: String,
        @SerializedName("image_url") val imageUrl: String,
        @SerializedName("gift_cards") val giftCards: List<GiftCard>
)