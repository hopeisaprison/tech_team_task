package com.hopeisaprison.ru.esctechtest.di.modules

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import javax.inject.Singleton


@Module
class NavigationModule(private val cicerone: Cicerone<Router>) {


    @Provides
    @Singleton
    fun router() = cicerone.router

    @Provides
    @Singleton
    fun navigatorHolder() = cicerone.navigatorHolder


}