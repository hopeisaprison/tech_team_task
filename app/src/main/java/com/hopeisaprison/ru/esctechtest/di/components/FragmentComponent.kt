package com.hopeisaprison.ru.esctechtest.di.components

import com.hopeisaprison.ru.esctechtest.di.PerActivity
import com.hopeisaprison.ru.esctechtest.di.PerFragment
import com.hopeisaprison.ru.esctechtest.di.modules.FragmentModule
import com.hopeisaprison.ru.esctechtest.ui.description.DescriptionFragment
import com.hopeisaprison.ru.esctechtest.ui.gifts.GiftsFragment
import dagger.Component
import javax.inject.Singleton

@PerFragment
@Component(modules = [FragmentModule::class], dependencies = [AppComponent::class])
interface FragmentComponent {

    fun inject(fragment: GiftsFragment)

    fun inject(fragment: DescriptionFragment)
}