package com.hopeisaprison.ru.esctechtest.data.network.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GiftCard (
        @SerializedName("id") val id : Int,
        @SerializedName("featured") val featured: Boolean,
        @SerializedName("title") val title: String,
        @SerializedName("credits") val credits: Int,
        @SerializedName("image_url") val imageUrl: String,
        @SerializedName("codes_count") val codesCount: Int,
        @SerializedName("currency") val currency: String,
        @SerializedName("description") val description: String,
        @SerializedName("redeem_url") val redeemUrl: String
) : Serializable