package com.hopeisaprison.ru.esctechtest.data.network

import com.hopeisaprison.ru.esctechtest.BuildConfig
import com.hopeisaprison.ru.esctechtest.data.network.model.ApiAnswer
import com.hopeisaprison.ru.esctechtest.data.network.model.GiftProvider
import io.reactivex.Single
import retrofit2.http.GET

interface GiftsApi {

    @GET(BuildConfig.PROVIDERS_PATH)
    fun getGiftProviders(): Single<ApiAnswer>
}