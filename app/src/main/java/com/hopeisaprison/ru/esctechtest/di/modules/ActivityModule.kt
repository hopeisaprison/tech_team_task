package com.hopeisaprison.ru.esctechtest.di.modules

import android.content.Context
import com.hopeisaprison.ru.esctechtest.data.network.ApiEndPoint
import com.hopeisaprison.ru.esctechtest.data.network.GiftsApi
import com.hopeisaprison.ru.esctechtest.di.PerActivity
import com.hopeisaprison.ru.esctechtest.di.PerFragment
import com.hopeisaprison.ru.esctechtest.ui.main.MainPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module()
class ActivityModule() {



    @Provides
    @PerActivity
    fun compositeDisposable() = CompositeDisposable()

}