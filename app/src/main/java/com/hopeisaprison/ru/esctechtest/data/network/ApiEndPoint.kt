package com.hopeisaprison.ru.esctechtest.data.network

import com.hopeisaprison.ru.esctechtest.BuildConfig

class ApiEndPoint {

    companion object {
        val API_URL = BuildConfig.API_URL
    }
}